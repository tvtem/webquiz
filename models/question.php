<?php

if (!isset($fromRoot) || !$fromRoot){
    require_once('../lib/core.php');
}

?>

<div class="col-xs-12">
<question>
<input name="qid" type="hidden" value="<?=uniqueSmallID()?>">
<div class="card section-tabs">
    <div class="row">
        <div class="col-xs-12">
            <div class="question-header">
                <i class="question-toggler material-icons">arrow_drop_up</i>
                Quadro de Pergunta

                <span class="pull-right question-remover">
                    <i class="material-icons">remove_circle</i>
                    REMOVER
                </span>
            </div>
        </div>
        <div class="col-xs-12 toggled-only">
            <div class="content">
                <h4 id="question-title"></h4>
            </div>
        </div>
        <div class="not-toggled-only">
            <div class="col-xs-12 col-md-6">
                <div class="content">
                    <div class="form-group label-floating empty">
                        <label class="control-label">Título da Pergunta</label>
                        <input name="title" type="text" class="form-control">
                        <span class="material-input"></span>
                    </div>
                    <div class="dz-box">
                        <form class="dropzone">
                            <div class="bg-center dz-bg" data-img="">
                                <img src="<?=BASE_URL?>img/placeholders/5x3.png" width="100%" />
                                <div class="bg-center dz-bg-cover" style="background-image: url();"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="content">
                    <h3>Opções</h3>

                    <div class="form-group">
                    </div>

                </div>
            </div>
            <div class="col-xs-12">
                <div class="content">
                    <h3>Respostas:</h3>
                    <div class="row">
                        <answers>
                            <?php if (isset($fromRoot)) { include('models/answer.php'); } else { include('answer.php'); } ?>
                            <?php if (isset($fromRoot)) { include('models/answer.php'); } else { include('answer.php'); } ?>
                            
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <answeradd>
                                    <div class="hidden-xs" style="height: 155px;"></div>
                                    <button class="btn btn-primary btn-lg">
                                        <i class="material-icons">add</i> Adicionar Resposta
                                        <div class="ripple-container"></div>
                                    </button>
                                    <div class="hidden-xs" style="height: 155px;"></div>
                                </answeradd>
                            </div>
                        </answers>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</question>
</div>
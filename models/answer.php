<?php

if (!isset($fromRoot) || !$fromRoot){
    require_once('../lib/core.php');
}

?>

<div class="col-xs-12 col-sm-6 col-md-4">
    <answer>
        <input name="aid" type="hidden" value="<?=uniqueSmallID()?>">
        <div class="card section-tabs">
            <span class="pull-right answer-remover">
                <i class="material-icons">remove_circle</i>
            </span>

            <div class="content">
                <div class="form-group label-floating empty">
                    <label class="control-label">Título da Resposta</label>
                    <input name="title" type="text" class="form-control">
                    <span class="material-input"></span>
                </div>
                <div class="dz-box">
                    <form class="dropzone">
                        <div class="bg-center dz-bg" data-img="">
                            <img src="<?=BASE_URL?>img/placeholders/5x3.png" width="100%" />
                            <div class="bg-center dz-bg-cover" style="background-image: url();"></div>
                        </div>
                    </form>
                </div>
                <div class="form-group">
                    <div class="togglebutton">
                        <label>
                            <input type="checkbox" id="iscorrect" name="answer-correct">
                            Resposta Correta
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </answer>
</div>
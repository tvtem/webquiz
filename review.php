<?php

require_once('master/header.php');
prepareTemplate('quiz-page');


if (!isLogged()){
    header('Location: '. BASE_URL . 'login.php?p=' . getCurrentUrl());
    exit;
}

$qid = __($_GET['id']);

$content = getOne('quiz', $qid);

$quizData = json_decode($content['content']);
$cover_img = $quizData->cover_img;

$info = getQuizGeneralInfo($qid);
$rank = getRankInfo($qid);
$most = getMostFromRank($rank);


?>
<div class="header header-filter" style="background-image: url('<?=$cover_img;?>');"></div>

<div class="main main-raised">
    <div class="profile-content">
        <div class="container" id="review">
            <div class="section">
                <h1 class="text-center">Relatório de dados<br/><?=$quizData->title?></h1>
            </div>
            <div class="section">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-5">
                        <h3 class="text-center">Informações Gerais</h3>

                        <ul class="nav nav-pills nav-pills-primary rv-box" role="tablist">
                            <li>
                                <a href="#dashboard" role="tab" data-toggle="tab">
                                    <div class="num"><?=$info->users?></div>
                                    Responderam
                                </a>
                            </li>
                            <li class="active">
                                <a href="#schedule" role="tab" data-toggle="tab">
                                    <div class="num">
                                        <?=$info->hits?>
                                    </div>

                                    Nota média
                                </a>
                            </li>
                            <li>
                                <a href="#tasks" role="tab" data-toggle="tab">
                                    <div class="num">
                                        <?=$info->time->format('%i')?><span class="small">m</span>
                                        <?=$info->time->format('%s')?><span class="small">s</span>
                                    </div>
                                    Tempo Médio
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-7">
                        <h3 class="text-center">Ranking</h3>

                        <table class="table rv-ranking">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>Nome</th>
                                    <th>Acertos</th>
                                    <th>Tempo</th>
                                    <th class="text-right">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count = 0;
                                foreach ($rank as $userData){
                                    //var_dump($userData);

                                    if ($count >= 3){
                                        break;
                                    }
                                ?>
                                <tr>
                                    <td class="text-center"><?=$count==0?'<i class="fa fa-trophy"></i>':$count+1?></td>
                                    <td><?=$userData['name']?></td>
                                    <td><?=$userData['hits']?></td>
                                    <td><?=zf($userData['time']->format('%h'), 2)?>:<?=zf($userData['time']->format('%i'), 2)?>:<?=zf($userData['time']->format('%s'), 2)?></td>
                                    <td class="td-actions text-right">
                                        <button type="button" rel="tooltip" title="Ver resultados" class="btn btn-info btn-simple btn-xs">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </td>
                                </tr>
                                <?php
                                    $count++;
                                }

                                ?>
                            </tbody>
                        </table>
                        
                        <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#rankModal">
                            <i class="material-icons">present_to_all</i> Ver Todos
                        </button>


                        <!-- Modal Core -->
                        <div class="modal fade" id="rankModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">RANKING</h4>
                                </div>
                                <div class="modal-body">
                                    <table class="table rv-ranking">
                                        <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th>Nome</th>
                                                <th>Acertos</th>
                                                <th>Tempo</th>
                                                <th class="text-right">Ações</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $count = 0;
                                            foreach ($rank as $userData){
                                                //var_dump($userData);
                                            ?>
                                            <tr>
                                                <td class="text-center"><?=$count==0?'<i class="fa fa-trophy"></i>':$count+1?></td>
                                                <td><?=$userData['name']?></td>
                                                <td><?=$userData['hits']?></td>
                                                <td><?=zf($userData['time']->format('%h'), 2)?>:<?=zf($userData['time']->format('%i'), 2)?>:<?=zf($userData['time']->format('%s'), 2)?></td>
                                                <td class="td-actions text-right">
                                                    <button type="button" rel="tooltip" title="Ver resultados" class="btn btn-info btn-simple btn-xs">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <?php
                                                $count++;
                                            }

                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Fechar</button>
                                </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>   
            <div class="section">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="row">

                        <?php
                            $more = $most['more'];
                            
                            $questions = $quizData->questions;

                            foreach ($questions as $q){
                                if ($q->qid != $more['qid'])
                                    continue;

                        ?>
                            <h3 class="text-center">Pergunta mais acertada</h3>
                            <div class="col-xs-12">
                                <h3><strong><?=$q->title?></strong></h3>
                            </div>
                            <div class="col-xs-12">
                                <div class="qf-question" id="<?=$q->qid?>">
                                    <div class="qf-question-header">
                                        <img class="img-responsive" src="<?=$q->cover?>" width="100%">
                                    </div>
                                    <div class="row">
                                            <?php
                                                $answers = $q->answers;
                                                $acount = 0;
                                                foreach ($answers as $a){
                                                    $arcount = isset($more[$a->aid])?$more[$a->aid]:0;

                                            ?>

                                                <?php if ($a->cover != "") { ?>
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class="qf-answer <?=$a->correct?' qf-answer-correct':''?>" id="<?=$a->aid?>" data-toggle="tooltip" data-placement="top" title="<?=$arcount?> usuários escolheram esta.">
                                                                <?=$a->correct?'<div class="qf-badge qf-correct-badge"><i class="material-icons">check</i></div>':''?>
                                                                <div class="qf-answer-cover " style="background-image: url(<?=$a->cover?>)">
                                                                    <img src="img/placeholders/5x3.png" class="img-responsive" width="100%">
                                                                </div>

                                                            
                                                                <?php if ($a->title != "") { ?><h4 class="qf-answer-cover-subtitle"><?=$a->title?></h4> <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="col-xs-12 col-md-12">
                                                        <div class="qf-answer-line <?=$a->correct?' qf-answer-correct':''?>" id="<?=$a->aid?>" data-toggle="tooltip" data-placement="top" title="<?=$arcount?> usuários escolheram esta.">
                                                                <?=$a->correct?'<div class="qf-badge qf-correct-badge"><i class="material-icons">check</i></div>':''?>
                                                            <?php if ($a->title != "") { ?><h4 class="qf-answer-cover-subtitle"><?=$a->title?></h4> <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php 
                                                    } 
                                                }
                                            ?>

                                    </div>
                                </div>
                            </div>

                        <?php
                            }
                        ?>

                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <div class="row">
                        
                    <?php
                    $more = $most['less'];
                    
                    $questions = $quizData->questions;

                    foreach ($questions as $q){
                        if ($q->qid != $more['qid'])
                            continue;

                ?>
                    <h3 class="text-center">Pergunta mais errada</h3>
                    <div class="col-xs-12">
                        <h3><strong><?=$q->title?></strong></h3>
                    </div>
                    <div class="col-xs-12">
                        <div class="qf-question" id="<?=$q->qid?>">
                            <div class="qf-question-header">
                                <img class="img-responsive" src="<?=$q->cover?>" width="100%">
                            </div>
                            <div class="row">
                                    <?php
                                        $answers = $q->answers;
                                        $acount = 0;
                                        foreach ($answers as $a){
                                            $arcount = isset($more[$a->aid])?$more[$a->aid]:0;

                                    ?>

                                        <?php if ($a->cover != "") { ?>
                                            <div class="col-xs-12 col-md-6">
                                                <div class="qf-answer <?=$a->correct?' qf-answer-correct':''?>" id="<?=$a->aid?>" data-toggle="tooltip" data-placement="top" title="<?=$arcount?> usuários escolheram esta.">
                                                        <?=$a->correct?'<div class="qf-badge qf-correct-badge"><i class="material-icons">check</i></div>':''?>
                                                        <div class="qf-answer-cover " style="background-image: url(<?=$a->cover?>)">
                                                            <img src="img/placeholders/5x3.png" class="img-responsive" width="100%">
                                                        </div>

                                                    
                                                        <?php if ($a->title != "") { ?><h4 class="qf-answer-cover-subtitle"><?=$a->title?></h4> <?php } ?>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-xs-12 col-md-12">
                                                <div class="qf-answer-line <?=$a->correct?' qf-answer-correct':''?>" id="<?=$a->aid?>" data-toggle="tooltip" data-placement="top" title="<?=$arcount?> usuários escolheram esta.">
                                                        <?=$a->correct?'<div class="qf-badge qf-correct-badge"><i class="material-icons">check</i></div>':''?>
                                                    <?php if ($a->title != "") { ?><h4 class="qf-answer-cover-subtitle"><?=$a->title?></h4> <?php } ?>
                                                </div>
                                            </div>
                                        <?php 
                                            } 
                                        }
                                    ?>

                            </div>
                        </div>
                        </div>
                    </div>

                <?php
                    }
                ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

require_once('master/footer.php');

?>
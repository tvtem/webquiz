<?php

require_once('master/header.php');

?>

	<div class="wrapper">
		<div class="header header-filter" style="background-image: url('img/city.jpg'); background-size: cover; background-position: top center;">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">

						<div style="height: 100px;" class="hidden-xs"></div>

						<div class="card card-signup">
							<form class="form" action="dologin.php" success="core.showNotification(core.requestData.msg, core.requestData.icon, core.requestData.type);if (core.requestData.error != 'true'){setTimeout(function(){ if (core.requestData.first_login == 1){ core.loadPage('/chgpassword'); } else {core.loadPage('<?php if (isset($_GET['p'])) { echo __($_GET['p']); } else { echo '/'; }?>');}}, 2000); }" fail="core.showNotification('Não foi possível fazer login. (Código 1)', 'error', 'danger');" rest>
								<div class="header header-primary text-center">
									<h4>Login</h4>
								</div>
								<div class="col-xs-10 col-xs-offset-1">

									<div class="form-group label-floating">
										<label class="control-label">Usuário ou CPF (Apenas Números)</label>
										<input type="text" name="user" class="form-control" required>
									</div>

									<div class="form-group label-floating">
										<label class="control-label">Senha</label>
										<input type="password" name="password" class="form-control" required>
									</div>
								</div>
								<div class="footer text-center">
									<div class="input-group" style="width: 100%;padding: 0 31px 0 30px;">
										<p>&nbsp;</p>
										<input type="submit" class="btn btn-primary btn-block" value="Entrar" />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>

<?php

require_once('master/footer.php');

?>
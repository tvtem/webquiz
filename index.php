<?php
require_once('lib/core.php');

if (isLogged()){
	require_once('quizzes.php');
	exit;
}


require_once('master/header.php');

?>

<div class="wrapper">
	<div class="header header-filter" style="background-image: url('img/city.jpg'); transform: translate3d(0px, 0px, 0px);">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="brand">
						<h1>Tem Quiz</h1>
						<h3>Um sistema de interatividade com perguntas e respostas.</h3>
					</div>
				</div>
			</div>

		</div>
	</div>

<?php

require_once('master/footer.php');

?>
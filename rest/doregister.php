<?php

require_once('../lib/core.php');
header('Content-type: application/json');

$user = ''; 
$pass = '';
$rpass = '';
$name = '';
$esamc = '';
$birth = '';
$mail = '';


if (isset($_POST['user']))
    $user = __($_POST['user']);

if (isset($_POST['password']))
    $pass = __($_POST['password']);

if (isset($_POST['rpassword']))
    $rpass = __($_POST['rpassword']);

if (isset($_POST['name']))
    $name = __($_POST['name']);

if (isset($_POST['esamc']))
    $esamc = __($_POST['esamc']);

if (isset($_POST['birth']))
    $birth = __($_POST['birth']);

if (isset($_POST['mail']))
    $mail = __($_POST['mail']);


if ($pass != $rpass){
    echo json_encode('{"error":"true", "msg": "As senhas não são iguais.", "icon": "error", "type": "danger"}');  
    exit;
}

$res = doRegister($user, $pass, $name, $esamc == 'on' ? 'ESAMC' : ' ', $birth, $mail);
if ($res){
    echo json_encode('{"msg": "Registrado com sucesso! Já pode fazer o login.", "icon": "done", "type": "success"}');  
    exit;
}

echo json_encode('{"error":"true", "msg": "Usuário já registrado. Por favor, escolha um novo usuário.", "icon": "error", "type": "danger"}');

?>
<?php

require_once('../lib/core.php');
header('Content-type: application/json');

$user = '';
$pass = '';

if (isset($_POST['user']))
    $user = __($_POST['user']);
if (isset($_POST['password']))
    $pass = __($_POST['password']);

$res = doLogin($user, $pass);
if ($res){
    $user = getUserId();
    $first_login = 0;
    if ($_SESSION[SERVER_IDENT]['update_time'] == ""){
        $first_login = 1;
        echo json_encode('{"first_login": '.$first_login.', "msg": "Primeiro acesso efetuado. Por favor, altere sua senha!", "icon": "done", "type": "success"}');
    } else {
        echo json_encode('{"first_login": '.$first_login.', "msg": "Login efetuado com sucesso!", "icon": "done", "type": "success"}');
    }
    exit;
}

echo json_encode('{"error":"true", "msg": "Usuário ou senha inválida.", "icon": "error", "type": "danger"}');

?>
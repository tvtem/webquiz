<?php

require_once('../lib/core.php');
header('Content-type: application/json');

$res = doLogout();
if ($res){
    echo json_encode('{"msg": "Logout efetuado com sucesso!", "icon": "done", "type": "success"}');  
    exit;
}

echo json_encode('{"msg": "Não foi possível efetuar Logout.", "icon": "error", "type": "danger"}');

?>
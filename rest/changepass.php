<?php

require_once('../lib/core.php');
header('Content-type: application/json');

$user = '';
$pass = '';
$npass = '';
$rnpass = '';

if (isset($_POST['user']))
    $user = __($_POST['user']);

if (isset($_POST['password']))
    $pass = __($_POST['password']);

if (isset($_POST['npassword']))
    $npass = __($_POST['npassword']);

if (isset($_POST['rnpassword']))
    $rnpass = __($_POST['rnpassword']);

$login = doLogin($user, $pass);
if ($login){
    doLogout();

    if ($npass != $rnpass){
        echo json_encode('{"error":"true", "msg": "Novas senhas não são iguais.", "icon": "error", "type": "danger"}');  
        exit;
    }

    if ($pass == $npass){
        echo json_encode('{"error":"true", "msg": "Nova senha não deve ser igual à antiga.", "icon": "error", "type": "danger"}');  
        exit;
    }

    $res = changePass($user, $npass);
    update('account', array('update_time'), array(time('U')), 'username', $user);
    if ($res){
        echo json_encode('{"msg": "Senha alterada com sucesso!", "icon": "done", "type": "success"}');  
        exit;
    }

    echo json_encode('{"error":"true", "msg": "Usuário ou senha inválidos.", "icon": "error", "type": "danger"}');
    exit;
} 

echo json_encode('{"error":"true", "msg": "Usuário ou senha inválidos.", "icon": "error", "type": "danger"}');

?>
<?php

require_once('../lib/core.php');

if (isLogged()){

    $data = json_encode($_POST);
    $content = json_decode($data);

    $fields = array('user', 'quiz', 'start', 'end', 'hits', 'total', 'data', 'time');
    $values = array(getUserId(), $content->qid, $content->start, $content->end, $content->hits, sizeof($_POST['qdata']), $data, $content->time);

    $result = execRaw("SELECT id FROM quiz_result WHERE quiz='".$content->qid."' AND user='".getUserId()."'");
    if ($result->num_rows > 0){
        die('UNSUCCESS');
    } else {
        post('quiz_result', $fields, $values);
        die('SUCCESS');
    }


} else {

    die('NOT_AUTHORIZED');

}

?>
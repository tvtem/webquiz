<?php

require_once('../lib/core.php');

$data = '';
if (isset($_POST['data'])){
    $data = json_encode($_POST['data']);
}

$time = ceil(__($_POST['time']));
$quiz = __($_POST['quiz']);

$session = array(
    'time' => $time,
    'data' => $data,
    'quiz' => $quiz
);

updateCurrentSession($session);

die('SUCCESS');


?>
<?php

require_once('../lib/core.php');

if (isAdmin()){
        
    $content = ___($_POST['content']);
    $id = __($_POST['id']);

    $updated_time = time('U');

    $fields = array('id', 'updated_time', 'user', 'content');
    $values = array($id, $updated_time, $_SESSION[SERVER_IDENT]['id'], $content);

    $pub = __($_POST['publish']);
    if ($pub == "true"){
        array_push($fields, 'published');
        array_push($values, '1');
    }

    update('quiz', $fields, $values, 'id', $id);
    $bkp = post('data_history', array('type', 'data'), array('quiz', $content));
    

    die('SUCCESS');

} else {

    die('NOT_AUTHORIZED');

}

?>
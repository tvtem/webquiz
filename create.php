<?php
require_once('master/header.php');


if (!isLogged()){
    header('Location: '. BASE_URL . 'login.php?p=' . getCurrentUrl());
    exit;
}


if (!isAdmin()){ 
    header('Location: ' . BASE_URL);
    exit;
}

if (!isset($_GET['id'])){
    die("WRONG_ID");
    exit;
}

$fromRoot = true;

$qid = __($_GET['id']);


if ($qid == "new" || isset($_GET['new'])){

    $qid = uniqueID();
    $time = time('U');
    $fields = array('id', 'created_time', 'user', 'published');
    $values = array($qid, $time, $_SESSION[SERVER_IDENT]['id'], 0);

    post('quiz', $fields, $values);
    
    header('Location: ' . BASE_URL . 'creator/' . $qid . '/');
    exit;

}

prepareTemplate('default-page');

$data = getWhere('quiz', '*', 'id', $qid);

$created_time = $data[0][0];
$published = $data[0][5];
$ncontent = $data[0][4];

$content = json_decode($ncontent);

?>
<div class="header header-filter" style="background-image: url('<?=BASE_URL?>img/examples/city.jpg');"></div>

<quiz id="createQuiz">
    <div class="main main-raised">
        <div class="profile-content">
            <div class="container">

                <?php
                    if (isset($content->id)){
                    /*
                        <div class="section section-sm">
                            <h3>Código (<?=$qid?>)</h3>
                            <div class="row">
                                <div class="col-xs-12">
                                    <textarea class="form-control" id="jsoncode"></textarea>
                                </div>
                            </div>
                        </div>
                    */
                        ?>

                        <div class="section section-sm">

                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <h3>Criar Novo Quiz</h3>
                                    <input type="hidden" name="id" id="qid" value="<?=$qid?>" />
                                    <input type="hidden" name="created_time" id="q_created_time" value="<?=$created_time?>" />
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <h3>Opções</h3>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <div class="dz-box" id="cover-img-dz">
                                        <input type="hidden" class="dz" name="cover-img" id="cover-img" value="" />
                                        <form class="dropzone">
                                            <div class="bg-center dz-bg" data-img="<?=$content->cover_img?>">
                                                <img src="<?=BASE_URL?>img/placeholders/5x3.png" width="100%" />
                                                <div class="bg-center dz-bg-cover" style="background-image: url(<?=$content->cover_img?>);"></div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-4">
                                    <div class="form-group label-floating empty">
                                        <label class="control-label">Título do Quiz</label>
                                        <input name="title" id="title" type="text" class="form-control" value="<?=$content->title?>">
                                        <span class="material-input"></span>
                                    </div>
                                    <div class="form-group label-floating empty">
                                        <label class="control-label">Descrição do Quiz</label>
                                        <textarea name="description" id="description" type="text" class="form-control ta"><?=$content->description?></textarea>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-4">
                                    <table class="table gridless table-middle">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" id="showcorrect" name="option-show-corrects" <?=$content->showcorrect ? "checked" : ""?>>
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>Mostrar acerto/erro na questão</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" id="showallcorrect" name="option-show-all-corrects" <?=$content->showallcorrect ? "checked" : ""?>>
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>Mostrar acertos/erros totais no final</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <?php
                                                        $time = 0;
                                                        if (isset($content->time)){
                                                            $time = $content->time;
                                                        }
                                                    ?>
                                                    <div class="form-group label-floating empty">
                                                        <label class="control-label">Tempo do Quiz (em milisegundos)</label>
                                                        <input name="time" id="time" type="text" class="form-control numonly" value="<?=$time?>" maxlength="12">
                                                        <span class="material-input"></span>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        

                        <div class="section section-sm">
                            <h3>Resultados do Quiz</h3>
                            <div class="row">
                                <results>
                                    <?php
                                        $results = $content->results;
                                        foreach ($results as $result){
                                            ?>
                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                <result>
                                                    <div class="card section-tabs">
                                                        <span class="pull-right result-remover">
                                                            <i class="material-icons">remove_circle</i>
                                                        </span>
                                                        <div class="content">
                                                            <div class="form-group label-floating empty">
                                                                <label class="control-label">Título do Resultado</label>
                                                                <input name="title" type="text" class="form-control" value="<?=$result->title?>">
                                                                <span class="material-input"></span>
                                                            </div>
                                                            <div class="dz-box">
                                                                <form class="dropzone">
                                                                    <div class="bg-center dz-bg" data-img="<?=$result->cover?>">
                                                                        <img src="<?=BASE_URL?>img/placeholders/5x3.png" width="100%" />
                                                                        <div class="bg-center dz-bg-cover" style="background-image: url(<?=$result->cover?>);"></div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <div class="form-group label-floating empty">
                                                                <label class="control-label">Quantidade de Acertos</label>
                                                                <input name="hits" type="text" class="form-control numonly" value="<?=$result->hits?>">
                                                                <span class="material-input"></span>
                                                            </div>
                                                            <div class="form-group label-floating empty">
                                                                <label class="control-label">Descrição do Resultado</label>
                                                                <textarea name="description" type="text" class="form-control ta"><?=$result->description?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </result>
                                            </div>
                                            <?php
                                        }

                                    ?>
                                    

                                    
                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                        <resultadd>
                                            <div class="hidden-xs" style="height: 155px;"></div>
                                            <button class="btn btn-primary btn-lg">
                                                <i class="material-icons">add</i> Adicionar Resultado
                                                <div class="ripple-container"></div>
                                            </button>
                                        </resultadd>
                                    </div>
                                </results>

                            </div>
                        </div>

                        <hr >

                        <div class="section section-sm">
                            <h3>Perguntas</h3>
                            <div class="row">
                                <questions>
                                    <?php
                                        $count = 0;
                                        $questions = $content->questions;
                                        foreach ($questions as $question){

                                        ?>
                                        <div class="col-xs-12">
                                            <question>
                                                <input name="qid" type="hidden" value="<?=$question->qid?>">
                                                <div class="card section-tabs">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="question-header">
                                                                <i class="question-toggler material-icons">arrow_drop_up</i>
                                                                Quadro de Pergunta

                                                                <span class="pull-right question-remover">
                                                                    <i class="material-icons">remove_circle</i>
                                                                    REMOVER
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 toggled-only">
                                                            <div class="content">
                                                                <h4 id="question-title"></h4>
                                                            </div>
                                                        </div>
                                                        <div class="not-toggled-only">
                                                            <div class="col-xs-12 col-md-6">
                                                                <div class="content">
                                                                    <div class="form-group label-floating empty">
                                                                        <label class="control-label">Título da Pergunta</label>
                                                                        <input name="title" type="text" class="form-control" value="<?=$question->title?>">
                                                                        <span class="material-input"></span>
                                                                    </div>
                                                                    <div class="dz-box">
                                                                        <form class="dropzone">
                                                                            <div class="bg-center dz-bg" data-img="<?=$question->cover?>">
                                                                                <img src="<?=BASE_URL?>img/placeholders/5x3.png" width="100%" />
                                                                                <div class="bg-center dz-bg-cover" style="background-image: url(<?=$question->cover?>);"></div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-6">
                                                                <div class="content">
                                                                    <h3>Opções</h3>

                                                                    <div class="form-group">
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <div class="content">
                                                                    <h3>Respostas:</h3>
                                                                    <div class="row">
                                                                        <answers>
                                                                        <?php
                                                                            $answers = $question->answers;
                                                                            foreach ($answers as $answer){
                                                                            ?>
                                                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                                                <answer>
                                                                                    <input name="aid" type="hidden" value="<?=$answer->aid?>">
                                                                                    <div class="card section-tabs">
                                                                                        <span class="pull-right answer-remover">
                                                                                            <i class="material-icons">remove_circle</i>
                                                                                        </span>
                                                                                        <div class="content">
                                                                                            <div class="form-group label-floating empty">
                                                                                                <label class="control-label">Título da Resposta</label>
                                                                                                <input  name="title" type="text" class="form-control" value="<?=$answer->title?>">
                                                                                                <span class="material-input"></span>
                                                                                            </div>
                                                                                            <div class="dz-box">
                                                                                                <form class="dropzone">
                                                                                                    <div class="bg-center dz-bg" data-img="<?=$answer->cover?>">
                                                                                                        <img src="<?=BASE_URL?>img/placeholders/5x3.png" width="100%" />
                                                                                                        <div class="bg-center dz-bg-cover" style="background-image: url(<?=$answer->cover?>);"></div>
                                                                                                    </div>
                                                                                                </form>
                                                                                            </div>

                                                                                            <div class="form-group">
                                                                                                <div class="togglebutton">
                                                                                                    <label>
                                                                                                        <input type="checkbox" id="iscorrect" name="answer-correct" <?=$answer->correct ? "checked" : ""?>>
                                                                                                        Resposta Correta
                                                                                                    </label>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </answer>
                                                                            </div>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                            
                                                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                                                <answeradd>
                                                                                    <div class="hidden-xs" style="height: 155px;"></div>
                                                                                    <button class="btn btn-primary btn-lg">
                                                                                        <i class="material-icons">add</i> Adicionar Resposta
                                                                                        <div class="ripple-container"></div>
                                                                                    </button>
                                                                                    <div class="hidden-xs" style="height: 155px;"></div>
                                                                                </answeradd>
                                                                            </div>
                                                                        </answers>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </question>
                                        </div>

                                        <?php
                                            $count++;
                                        }
                                        ?>
                                    
                                                                
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <questionadd>
                                            <div class="hidden-xs" style="height: 25px;"></div>
                                            <button class="btn btn-primary btn-lg">
                                                <i class="material-icons">add</i> Adicionar Pergunta
                                                <div class="ripple-container"></div>
                                            </button>
                                            <div class="hidden-xs" style="height: 25px;"></div>
                                        </questionadd>
                                    </div>
                                </questions>
                            </div>
                        </div>

                        
                        <div class="section section-sm">
                            <div class="row">
                            <?php

                            if ($published == 1){
                                ?>
                                    <div class="col-xs-12 text-right">
                                        <button id="saveBtn" class="btn btn-primary btn-lg">
                                            <i class="material-icons">publish</i> Salvar
                                            <div class="ripple-container"></div>
                                        </button>
                                    </div>
            
                                <?php
                            
                            } else {
                                ?>
                                    <div class="col-xs-12 text-right">
                                        <button id="draftBtn" class="btn btn-info btn-lg">
                                            <i class="material-icons">save</i> Salvar Rascunho
                                            <div class="ripple-container"></div>
                                        </button>
                                        <button id="publishBtn" class="btn btn-primary btn-lg">
                                            <i class="material-icons">publish</i> Publicar
                                            <div class="ripple-container"></div>
                                        </button>
                                    </div>

                                <?php

                            }

                            ?>
                            </div>
                        </div>


                        <?php

                    } else {
                        /*

                        

                <div class="section section-sm">
                    <h3>Código (<?=$qid?>)</h3>
                    <div class="row">
                        <div class="col-xs-12">
                            <textarea class="form-control" id="jsoncode"></textarea>
                        </div>
                    </div>
                </div>


                        */
                ?>
                <div class="section section-sm">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <h3>Criar Novo Quiz</h3>
                            <input type="hidden" name="id" id="qid" value="<?=$qid?>" />
                            <input type="hidden" name="created_time" id="q_created_time" value="<?=$created_time?>" />
                        </div>
                        <div class="col-xs-12 col-sm-4">
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <h3>Opções</h3>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <div class="dz-box" id="cover-img-dz">
                                <input type="hidden" class="dz" name="cover-img" id="cover-img" value="" />
                                <form class="dropzone">
                                    <div class="bg-center dz-bg" data-img="">
                                        <img src="<?=BASE_URL?>img/placeholders/5x3.png" width="100%" />
                                        <div class="bg-center dz-bg-cover" style="background-image: url();"></div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group label-floating empty">
                                <label class="control-label">Título do Quiz</label>
                                <input name="title" id="title" type="text" class="form-control">
                                <span class="material-input"></span>
                            </div>
                            <div class="form-group label-floating empty">
                                <label class="control-label">Descrição do Quiz</label>
                                <textarea name="description" id="description" type="text" class="form-control ta"></textarea>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <table class="table gridless table-middle">
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" id="showcorrect" name="option-show-corrects" checked>
                                                </label>
                                            </div>
                                        </td>
                                        <td>Mostrar acerto/erro na questão</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" id="showallcorrect" name="option-show-all-corrects" checked>
                                                </label>
                                            </div>
                                        </td>
                                        <td>Mostrar acertos/erros totais no final</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="form-group label-floating empty">
                                                <label class="control-label">Tempo do Quiz (em milisegundos)</label>
                                                <input name="time" id="time" type="text" class="form-control numonly" value="600000" maxlength="12">
                                                <span class="material-input"></span>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="section section-sm">
                    <h3>Resultados do Quiz</h3>
                    <div class="row">
                        <results>
                            <?php include('models/result.php'); ?>

                            
                            <div class="col-xs-12 col-sm-4 col-md-3">
                                <resultadd>
                                    <div class="hidden-xs" style="height: 155px;"></div>
                                    <button class="btn btn-primary btn-lg">
                                        <i class="material-icons">add</i> Adicionar Resultado
                                        <div class="ripple-container"></div>
                                    </button>
                                </resultadd>
                            </div>
                        </results>

                    </div>
                </div>

                <hr >

                <div class="section section-sm">
                    <h3>Perguntas</h3>
                    <div class="row">
                        <questions>
                            <?php include('models/question.php'); ?>
                            
                                                        
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <questionadd>
                                    <div class="hidden-xs" style="height: 25px;"></div>
                                    <button class="btn btn-primary btn-lg">
                                        <i class="material-icons">add</i> Adicionar Pergunta
                                        <div class="ripple-container"></div>
                                    </button>
                                    <div class="hidden-xs" style="height: 25px;"></div>
                                </questionadd>
                            </div>
                        </questions>
                    </div>
                </div>

                <div class="section section-sm">
                    <div class="row">
                    <?php

                    if ($published == 1){
                        ?>
                            <div class="col-xs-12 text-right">
                                <button id="saveBtn" class="btn btn-primary btn-lg">
                                    <i class="material-icons">publish</i> Salvar
                                    <div class="ripple-container"></div>
                                </button>
                            </div>
    
                        <?php
                    
                    } else {
                        ?>
                            <div class="col-xs-12 text-right">
                                <button id="draftBtn" class="btn btn-info btn-lg">
                                    <i class="material-icons">save</i> Salvar Rascunho
                                    <div class="ripple-container"></div>
                                </button>
                                <button id="publishBtn" class="btn btn-primary btn-lg">
                                    <i class="material-icons">publish</i> Publicar
                                    <div class="ripple-container"></div>
                                </button>
                            </div>

                        <?php

                    }

                    ?>
                    </div>
                </div>

                <?php
                    }
                ?>
            </div>
        </div>
    </div>
</quiz>

<?php

require_once('master/footer.php');

?>
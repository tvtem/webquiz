<?php

require_once('master/header.php');
prepareTemplate('default-page');

?>
<div class="header header-filter" style="background-image: url('img/examples/city.jpg');"></div>

<div class="main main-raised">
    <div class="profile-content">
        <div class="container">
            <div class="section" id="allQuiz">
                <center><h1>Todos os Quizes</h1></center>
                <div class="row">
                <?php
                    
                    $filterGroup = '';
                    if ($_SESSION[SERVER_IDENT]['group'] > 0){
                        $filterGroup = " AND `group`='" . $_SESSION[SERVER_IDENT]['group'] . "'";
                    }

                    $qry = "SELECT * FROM quiz WHERE `published`='1' $filterGroup ORDER BY updated_time DESC";

                    $raw = execRaw($qry);
                    $quizzes = array();
                    while ($row = $raw->fetch_array()) {
                        array_push($quizzes, $row);
                    }

                    foreach ($quizzes as $quiz){
                        if ($quiz['content'] != NULL){
                            $data = json_decode($quiz['content'], true);
                            ?>

                                <div class="col-xs-12">
                                    <div class="card card-nav-tabs">
                                        <div class="headering header-info">
                                            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
                                            <ul class="nav nav-tabs">
                                                <li>
                                                    <a href="./<?=$quiz['id']?>/">
                                                        <i class="material-icons">spellcheck</i>
                                                        Responder Agora
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="content">
                                            <div class="tab-content">
                                                <div class="tab-pane active">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-4">
                                                            <div class="dz-box">
                                                                <div class="bg-center dz-bg" style="background-image: url(<?=$data['cover_img']?>);">
                                                                    <img src="<?=BASE_URL?>img/placeholders/5x3.png" width="100%" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 col-sm-7">
                                                            <h3><?=$data['title']?></h3>
                                                            <p><?=$data['description']?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            <?php
                        }
                    }

                ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

require_once('master/footer.php');

?>
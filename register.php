<?php

require_once('master/header.php');

?>

	<div class="wrapper">
		<div class="header header-filter" style="background-image: url('img/city.jpg'); background-size: cover; background-position: top center;">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-sm-6 col-sm-offset-3">

						<div style="height: 100px;" class="hidden-xs"></div>

						<div class="card card-signup">
							<form class="form" action="doregister.php" success="core.showNotification(core.requestData.msg, core.requestData.icon, core.requestData.type);if (core.requestData.error != 'true'){setTimeout(function(){ core.loadPage('/login'); }, 2000); }" fail="core.showNotification('Não foi possível registrar-se. (Código 1)', 'error', 'danger');" rest>
								<div class="header header-primary text-center">
									<h4>Registrar-se</h4>
								</div>
								<div class="col-xs-12 col-md-offset-1 col-md-5">

									<div class="form-group label-floating">
										<label class="control-label">Nome Completo</label>
										<input type="text" name="name" class="form-control" required>
									</div>

									<div class="form-group label-floating">
										<label class="control-label">Data de Nascimento</label>
										<input type="text" name="birth" class="form-control" required>
									</div>

									<div class="form-group label-floating">
										<label class="control-label">Email</label>
										<input type="text" name="mail" class="form-control" required>
									</div>


								</div>
								<div class="col-xs-12 col-md-5">

									<div class="form-group label-floating">
										<label class="control-label">CPF (Apenas Números)</label>
										<input type="text" name="user" class="form-control" required>
									</div>

									<div class="form-group label-floating">
										<label class="control-label">Senha</label>
										<input type="password" name="password" class="form-control" required>
									</div>

									<div class="form-group label-floating">
										<label class="control-label">Repetir Senha</label>
										<input type="password" name="rpassword" class="form-control" required>
									</div>

								</div>

								<div class="col-xs-12 col-md-offset-1 col-md-10 ">

									<div class="togglebutton">
									<label style="
										color: #000;
										padding-top: 15px;
										font-size: 16px;
										line-height: 10px;
									">
											<input id="esamc" type="checkbox" name="esamc">
											Aluno ESAMC?
										</label>
									</div>

									<div id="isEsamc" class="form-group label-floating hidden">
										<label class="control-label">Curso</label>
										<input type="text" name="course" class="form-control">
									</div>
								</div>
								<div class="footer text-center">
									<div class="input-group" style="width: 100%;padding: 0 31px 0 30px;">
										<p>&nbsp;</p>
										<input type="submit" class="btn btn-primary btn-block" value="Registrar-se" />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php

require_once('master/footer.php');

?>
	
<script>
	$('#esamc2').change(function(){
		var value = $(this).is(":checked")
		console.log(value)

		if (value){
			$('#isEsamc').removeClass('hidden')
		} else {
			$('#isEsamc').addClass('hidden')
		}

	});
</script>
<?php

# System Definitions
define ('SERVER_IDENT', 'com.tvtem.quiz.system');
define ('SESSION_DURA', 120);
define ('BASE_URL', 'https://quiz.temmais.com/');

# Start Session
session_start();

# Support Contact Info
define ('ADMIN_MAIL', 'adrian.prado@tvtem.com');
define ('ADMIN_NAME', 'Adrian Prado');
define ('ADMIN_FONE', '(15) 99633-1299');
define ('ADMIN_TITLE', 'Analista de Desenvolvimento Web');
define ('ADMIN_MSG',  '<br /><br /><br />Please contact the admin of the service.<hr><div><table class="MsoNormalTable" border="0" cellpadding="0"><tbody><tr><td style="padding: 0.75pt;"><p class="MsoNormal"><span class="object"><span style="color: rgb(51, 102, 153);"><span class="Object" role="link" id="OBJ_PREFIX_DWT2223_com_zimbra_url"><span class="Object" role="link" id="OBJ_PREFIX_DWT2259_com_zimbra_url"><a href="http://g1.globo.com/sao-paulo/sorocaba-jundiai/index.html" target="_blank"><span style="color: white; border: 1pt none windowtext; padding: 0cm; text-decoration: none;"><img border="0" id="_x0000_i1025" dfsrc="http://temmais.com/temp/assinatura/2015/logo_rede.jpg" src="http://temmais.com/temp/assinatura/2015/logo_rede.jpg" saveddisplaymode=""></span></a></span></span></span></span></p></td><td style="padding: 0.75pt;"><table class="MsoNormalTable" border="0" cellpadding="0"><tbody><tr><td style="padding: 0.75pt;"><div><p class="MsoNormal"><b><span style="font-size: 15pt; font-family: arial, sans-serif; color: rgb(105, 106, 107);">Adrian&nbsp;Fernandes</span></b><br><span style="font-size: 10.5pt;font-family: arial, sans-serif; color: rgb(105, 106, 107);">adrian.prado@tvtem.com</span><br><span style="font-size: 10.5pt; font-family: arial, sans-serif; color: rgb(105, 106, 107);">Analista de Tecnologia Web | Sorocaba</span><br><span style="font-size: 10.5pt; font-family: arial, sans-serif; color: rgb(105, 106, 107);">15. &nbsp; 3224-8774</span><br /><span style="font-size: 10.5pt; font-family: arial, sans-serif; color: rgb(105, 106, 107);">15.&nbsp;99633-1299</span></p></div></td></tr></tbody></table></td></tr></tbody></table></div>');

# Database Configuration
define ('DB_HOST', '35.230.107.215');
define ('DB_USER', 'test');
define ('DB_PASS', 'test');
define ('DB_BASE', 'quiz');

# Connect to mysql server
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_BASE);
$GLOBALS['mysqli'] = $mysqli; 

if( $mysqli ) {

    # All right!

}else{
     kill("Connection could not be established.", print_r( $mysqli->error(), true));
}

function anti_injection($sql){
   $sql = preg_replace("/( from |select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/", "" ,$sql);
   $sql = trim($sql);
   $sql = strip_tags($sql);
   $sql = (get_magic_quotes_gpc()) ? $sql : addslashes($sql);
   return $sql;
}

function zf($input, $num){
    return str_pad($input, $num, "0", STR_PAD_LEFT);
}

function __($data){
    return anti_injection($data);
}

function ___($data){
    return mysqli_real_escape_string($GLOBALS['mysqli'], $data);
}

function execRaw($qry){
    $mysqli = $GLOBALS['mysqli'];

    if ($qry == ''){
        throw new Exception('Invalid SQL');
    }


    if (!$result = $mysqli->query($qry)) {
        throw new Exception($mysqli->error);
    } else {
        return $result;
    }
}

function getOne($obj, $id){
  $mysqli = $GLOBALS['mysqli'];

  $qry = "SELECT * FROM $obj WHERE id='$id'";
  try{
      if ($obj == ''){
          throw new Exception('Invalid SQL');
      }

      if (!$result = $mysqli->query($qry)) {
          throw new Exception($mysqli->error);
      } else {
          if ($row = $result->fetch_array()) {
              return $row;
          }
      }
      return null;


  } catch (Exception $ex){
      return array('error' => $ex . '::('.$qry.')');
  }
}

function getAll($obj){
  $mysqli = $GLOBALS['mysqli'];

  $qry = "SELECT * FROM $obj";
  try{
      if ($obj == ''){
          throw new Exception('Invalid SQL');
      }
      $res = array();

      if (!$result = $mysqli->query($qry)) {
          throw new Exception($mysqli->error);
      } else {
          while ($row = $result->fetch_array()) {
              array_push($res, $row);
          }
      }
      return $res;


  } catch (Exception $ex){
      return array('error' => $ex . '::('.$qry.')');
  }
}

function getWhere($obj, $fields, $field, $value){
  $mysqli = $GLOBALS['mysqli'];
  $qry = "SELECT $fields FROM $obj WHERE $field='$value'";
  try{
      if ($obj == ''){
          throw new Exception('Invalid SQL');
      }
      $res = array();

      if (!$result = $mysqli->query($qry)) {
          throw new Exception($mysqli->error);
      } else {
          while ($row = $result->fetch_array()) {
              array_push($res, $row);
          }
      }
      return $res;
  } catch (Exception $ex){
      return array('error' => $ex . '::('.$qry.')');
  }
}

function getAllWhere($obj, $field, $value){
  $mysqli = $GLOBALS['mysqli'];
  $qry = "SELECT * FROM $obj WHERE $field='$value'";
  try{
      if ($obj == ''){
          throw new Exception('Invalid SQL');
      }
      $res = array();

      if (!$result = $mysqli->query($qry)) {
          throw new Exception($mysqli->error);
      } else {
          while ($row = $result->fetch_array()) {
              array_push($res, $row);
          }
      }
      return $res;
  } catch (Exception $ex){
      return array('error' => $ex . '::('.$qry.')');
  }
}

function post($obj, $fields, $values) {
  $mysqli = $GLOBALS['mysqli'];
  $qry = "INSERT INTO $obj ({0}) VALUES ({1})";
  try {
      if ($obj == '') {
          throw new Exception('Invalid SQL');
      }

      $f = implode(',', $fields);
      $v = "'". implode("','", $values) ."'";

      $qry = str_replace('{0}', $f, $qry);
      $qry = str_replace('{1}', $v, $qry);

      //die ($qry);

      if (!$result = $mysqli->query($qry)) {
          throw new Exception($mysqli->error);
      } else {
          return $mysqli->insert_id;
      }
  } catch (Exception $ex) {
      return array('error' => $ex . '::('.$qry.')');
  }
}

function update($obj, $fields, $values, $field, $value) {
  $mysqli = $GLOBALS['mysqli'];
  $qry = "UPDATE $obj SET {0} WHERE $field='$value'";
  try {
      if ($obj == '') {
          throw new Exception('Invalid SQL');
      }

      $sets = "";

      $scount = 0;
      foreach ($fields as $f){
          if ($scount > 0)
            $sets .= ",";
        $sets .= " `$f`='".$values[$scount]."'";
        $scount++;
      }

      $qry = str_replace('{0}', $sets, $qry);

      if (!$result = $mysqli->query($qry)) {
          throw new Exception($mysqli->error);
      } else {
          return $mysqli->insert_id;
      }
  } catch (Exception $ex) {
      return array('error' => $ex . '::('.$qry.')');
  }
}

# General Functions
function uniqid_base36($more_entropy=false) {
    $s = uniqid('', $more_entropy);
    if (!$more_entropy)
        return base_convert($s, 16, 36);
        
    $hex = substr($s, 0, 13);
    $dec = $s[13] . substr($s, 15); // skip the dot
    return base_convert($hex, 16, 36) . base_convert($dec, 10, 36);
}

function uniqueID(){
    $id = str_replace('.', '', uniqid(time('U'), true));

    return $id;

}

function uniqueSmallID(){

    return uniqid_base36(true);

}

# User/Login/Logout Functions
function doLogin($user, $pass){
    $mysqli = $GLOBALS['mysqli'];
    $qry = "SELECT * FROM account WHERE username='$user' and password='$pass'";
    try {
  
        //$qry .= " WHERE $field='$value'";
  
        //die ($qry);
        $res = array();
      
        if (!$result = $mysqli->query($qry)) {
            throw new Exception($mysqli->error);
        } else {
            while ($row = $result->fetch_array()) {
                array_push($res, $row);
            }
        }

        $val = (sizeof($res) > 0);

        if ($val){
            $_SESSION[SERVER_IDENT]['id'] = $res[0]['id'];
            $_SESSION[SERVER_IDENT]['name'] = $res[0]['name'];
            $_SESSION[SERVER_IDENT]['username'] = $res[0]['username'];
            $_SESSION[SERVER_IDENT]['status'] = $res[0]['status'];
            $_SESSION[SERVER_IDENT]['update_time'] = $res[0]['update_time'];
            $_SESSION[SERVER_IDENT]['group'] = $res[0]['group'];
        }

        return $val;
    } catch (Exception $ex) {
        //return array('error' => $ex . '::('.$qry.')');
        return false;
    }
}

function changePass($user, $npass){
    $mysqli = $GLOBALS['mysqli'];
    $qry = "UPDATE account SET password='$npass' WHERE username='$user'";
    try {
        
        if (!$result = $mysqli->query($qry)) {
            throw new Exception($mysqli->error);
        } else {
            return true;
        }

    } catch (Exception $ex) {
        //return array('error' => $ex . '::('.$qry.')');
        return false;
    }
}

# User Register
function doRegister($user, $pass, $name, $course, $birth, $mail){
    $mysqli = $GLOBALS['mysqli'];
    $now = time('U');
    $qry = "INSERT INTO account (`create_time`,`update_time`,`username`,`password`,`email`,`name`,`birth`,`status`,`group`,`course`) VALUES ('$now', '$now', '$user', '$pass', '$mail', '$name', '$birth', '0', '1', '$course')";
    try {
        //var_dump($qry);
        $pre = $mysqli->query("SELECT * FROM account WHERE username='$user' ");
        $rows = $pre->num_rows;
        if ($rows > 0){
            return false;
        }

        //die(var_dump($qry));
        $res = array();
      
        if (!$result = $mysqli->query($qry)) {
            throw new Exception($mysqli->error);
        }

        return $mysqli->insert_id;
        
    } catch (Exception $ex) {
        //return array('error' => $ex . '::('.$qry.')');
        return false;
    }
}

function isLogged(){
    return (isset($_SESSION[SERVER_IDENT]['username']));
}

function doLogout(){
    try
    {
        session_destroy();

        session_start();
        
        return true;
    }
    catch (Exception $ex){
        return false;
    }
}

function getUserId(){
    $dom = $_SESSION[SERVER_IDENT]['id'];
    return $dom;
}

function isAdmin(){
    return ($_SESSION[SERVER_IDENT]['status'] == 100);
}

# Quiz functions 

function getCurrentSession($qid){
    $result = execRaw("SELECT * FROM quiz_session WHERE user='".getUserId()."' AND quiz='$qid' ");


    if ($session = $result->fetch_array()) {
        return $session;
    } else {
        # Nenhuma sessão encontrada... Criando Uma nova...
        post('quiz_session', array('user', 'quiz', 'time'), array(getUserId(), $qid, '-999'));

        return getCurrentSession($qid);
    }
}

function updateCurrentSession($session){
    $result = execRaw("UPDATE quiz_session SET time='".$session['time']."', data='".$session['data']."' WHERE user='".getUserId()."' AND quiz='".$session['quiz']."' ");
    return $result;
}

function getQuizGeneralInfo($id){


    $mysqli = $GLOBALS['mysqli'];

    $totaltime = 0;
    $totalusers = 0;
    $totalhits = 0;

    $user_data = array();

    $result = $mysqli->query("SELECT * FROM quiz_result WHERE quiz='$id' ORDER BY id ASC");
    while ($line = $result->fetch_array()){
        if (!array_key_exists($line['user'], $user_data)){
            $user_data[$line['user']] = $line;
            $totalusers++;

            $time = $line['end'] - $line['start'];
            $totaltime += $time;

            $totalhits += $line['hits'];
        }
    }

    $tseconds = round($totaltime / $totalusers);
    $ttime = new DateIntervalEnhanced('PT'.$tseconds.'S');
    $ttime = $ttime->recalculate();

    /*printf("TOTAL TIME: ". $totaltime . '<br />');
    printf("TOTAL USERS: ". $totalusers . '<br />');
    printf("MID TIME: ". $tseconds . '<br />');
    printf("MID TIME FORMATED: ". $ttime->format('%i:%s'). '<br />');

    die();*/

    $midhits = round($totalhits / $totalusers, 1);


    $info = array(
        'time' => $ttime,
        'hits' => $midhits,
        'users' => $totalusers
    );

    return (object) $info;

}

function getRankInfo($id){

    $mysqli = $GLOBALS['mysqli'];
    $user_data = array();
    $quiz_data = json_decode(getWhere('quiz', '*', 'id', $id)[0]['content']);

    $result = $mysqli->query("SELECT * FROM quiz_result WHERE quiz='$id' ORDER BY hits DESC, time DESC");
    while ($line = $result->fetch_array()){
        if (!array_key_exists($line['user'], $user_data)){
            $user_data[$line['user']] = $line;
        }
    }

    foreach($user_data as $user){
        if ($quiz_data->time < $user['time']){
            $user['time'] = 0;
        }

        $midtime = new DateIntervalEnhanced('PT'.round(($quiz_data->time - $user['time']) /1000).'S');
        $user_data[$user['user']]['time'] = $midtime->recalculate();
        $user_data[$user['user']]['name'] = getWhere('account', 'name', 'id', $user['user'])[0]['name'];
    }

    return $user_data;

}

function getMostFromRank($rank){
    $mdata = array();

    $more = 0;
    $more_id = "";

    $less = 99999999;
    $less_id = "";

    foreach ($rank as $userData){

        $ddata = $userData['data'];
        $odata = json_decode($ddata, true);

        $uid = $userData['user'];
        $qdata = $odata['qdata'];

        foreach ($qdata as $q){
            $qid = $q['qid'];
            $aid = $q['aid'];

            $cid = isset($mdata[$qid]['cid']) ? $mdata[$qid]['cid'] : 0;
            $aaid = isset($mdata[$qid][$aid]) ? $mdata[$qid][$aid] : 0;

            if ($q['cid'] == '1')
                $cid++;
            $aaid++;

            $mdata[$qid][$aid] = $aaid;
            $mdata[$qid]['cid'] = $cid;
        }
    }

    // more hits
    foreach ($mdata as $k => $q){
        if ($q['cid'] > $more){
            $more = $q['cid'];
            $more_id = $k;
        }
    }

    // less hits
    foreach ($mdata as $k => $q){
        if ($q['cid'] < $less){
            $less = $q['cid'];
            $less_id = $k;
        }
    }

    $mdata[$more_id]['qid'] = $more_id;
    $mdata[$less_id]['qid'] = $less_id;

    return array(
        "more" => $mdata[$more_id],
        "less" => $mdata[$less_id]
    );
}

# System functions

class DateIntervalEnhanced extends DateInterval {

    public function recalculate()
    {
        $from = new DateTime;
        $to = clone $from;
        $to = $to->add($this);
        $diff = $from->diff($to);
        foreach ($diff as $k => $v) $this->$k = $v;
        return $this;
    }

}

function getCurrentUrl($full = true) {
    $pageURL = 'http';
     if (@$_SERVER["HTTPS"] == "on") {
         $pageURL .= "s";
    }
     $pageURL .= "://";
     $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
     return $pageURL;
}

function kill($msg, $data){
    echo $msg;

    if ($_SERVER['REMOTE_HOST'] != '::1')
        die (ADMIN_MSG);
    else 
        die ('<pre>'.$data.'</pre>');
}

# Template Functions
function prepareTemplate($mainTemplate = 'index-page'){
    printf('
        <script class="remove-at-load">
            document.addEventListener("DOMContentLoaded", function() {
                var script = document.createElement("script");
                script.setAttribute("class", "remove-at-load" );
                script.innerHTML = "$(\'body\').removeClass(\'index-page\').addClass(\''.$mainTemplate.'\');$(\'.remove-at-load\').remove();";
                document.body.appendChild(script);
            });
        </script>
    ');

}


?>
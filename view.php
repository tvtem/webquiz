<?php

require_once('master/header.php');
prepareTemplate('quiz-page');


if (!isLogged()){
    header('Location: '. BASE_URL . 'login.php?p=' . getCurrentUrl());
    exit;
}

$qid = __($_GET['id']);

$content = getOne('quiz', $qid);

$quizData = json_decode($content['content']);
$cover_img = $quizData->cover_img;


$session = getCurrentSession($qid);

if ($session['time'] == -999){
    $session['time'] = $quizData->time;
    updateCurrentSession($session);
}

?>
<div class="hidden">
    <draw><?=var_dump($session)?></draw>
    <time><?=$session['time']?></time>
    <data><?=$session['data']?></data>
    <user><?=$session['user']?></user>
</div>
<div class="header header-filter" style="background-image: url('<?=$cover_img;?>');"></div>

<div class="main main-raised">
    <div class="profile-content">
        <div class="container">
            <div class="section" id="aQuiz">
                <script id="xconfig" type="text/config">
                    <?=trim($content['content'])?>
                </script>
                <center>
                    <h1><?=$quizData->title?></h1>
                    <br />
                    <h5><?=$quizData->description?></h5>
                </center>

                <div class="row" id="quizContent">
                    <div id="quizContentData" class="col-xs-12 col-md-8">
                        <?php
                            $qcount = 1;

                            $questions = $quizData->questions;

                            foreach ($questions as $q){
                                ?>

                                <div class="qf-question" id="<?=$q->qid?>">
                                    <div class="qf-question-header">
                                        <h3><strong><?=$qcount?>. <?=$q->title?></strong></h3>
                                        <img class="img-responsive" src="<?=$q->cover?>" width="100%" />
                                    </div>

                                    <div class="row">
                                        <?php
                                            $answers = $q->answers;
                                            $acount = 0;
                                            foreach ($answers as $a){
                                        ?>

                                            <?php if ($a->cover != "") { ?>
                                                <div class="col-xs-12 col-md-6">
                                                    <div class="qf-answer" id="<?=$a->aid?>">
                                                        
                                                            <div class="qf-answer-cover" style="background-image: url(<?=$a->cover?>)">
                                                                <img src="img/placeholders/5x3.png" class="img-responsive" width="100%">
                                                            </div>

                                                        
                                                            <?php if ($a->title != "") { ?><h4 class="qf-answer-cover-subtitle"><?=$a->title?></h4> <?php } ?>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="col-xs-12 col-md-12">
                                                    <div class="qf-answer-line" id="<?=$a->aid?>">
                                                        <?php if ($a->title != "") { ?><h4 class="qf-answer-cover-subtitle"><?=$a->title?></h4> <?php } ?>
                                                    </div>
                                                </div>
                                            <?php 
                                                } 
                                            }
                                        ?>

                                    </div>
                                </div>

                                <?php

                                $qcount++;
                            }

                        ?>
                    </div>
                    <div id="quizContentSupport" class="hidden-xs col-md-4">
                        <h3><strong>Tempo</strong></h3>
                        <div class="clock" id="clock-counter">
                            <div class="label label-primary">Tempo restante:</div>
                            <span id="_hh" class="h">00</span>:
                            <span id="_mm" class="m">00</span>:
                            <span id="_ss" class="s">00</span>:
                            <span id="_ms" class="ms">000</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

require_once('master/footer.php');

?>

<script class="remove-at-load">
    core.loadConfigs();
</script>
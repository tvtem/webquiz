<?php

require_once('../lib/core.php');
header('Content-type: application/json');

// Nas versões do PHP anteriores a 4.1.0, $HTTP_POST_FILES deve ser utilizado ao invés
// de $_FILES.

$OUTPUT = array();

$id = uniqueID();

$name = $_FILES["file"]["name"];
$ex = explode(".", $name);
$ext = end($ex); # extra () to prevent notice

$uploaddir = '../img/upload/';
$uploaduid = $id .'.'. $ext;
$uploadfile = $uploaddir . $uploaduid;



if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
    //post('images', array('created_time', 'image', 'file'), array(date('Y-m-d H:i:s'), $id, $uploaduid));

    $OUTPUT['img'] = BASE_URL . 'img2/' . $id. '/';
} else {
    $OUTPUT['error'] = "Não foi possível fazer o upload. #".$_FILES["file"]["error"];
}

printf(json_encode($OUTPUT));

?>
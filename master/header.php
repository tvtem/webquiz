<?php

require_once('lib/core.php');

?><!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon.png">
	<link rel="icon" type="image/png" href="img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Tem Quiz</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

	<!-- Fonts and icons -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
    <link href="<?=BASE_URL?>css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?=BASE_URL?>css/material-kit.css?_=1.0.0.0" rel="stylesheet"/>
    <link href="<?=BASE_URL?>css/custom.css?_=<?=time('U')?>" rel="stylesheet"/>
</head>
<body class="index-page">
	<div id="loader" class="pgloader"><span class="spinner-g"></span></div>

<!-- Navbar will come here -->
<nav class="navbar navbar-fixed-top navbar-color-on-scroll navbar-transparent">
	<div class="container">
        <div class="navbar-header">
	    	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-index">
	        	<span class="sr-only">Toggle navigation</span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	    	</button>
	    	<a href="#">
	        	<div class="logo-container">
	                <div class="logo">
	                    <img src="<?=BASE_URL?>img/logo.png" alt="Logo" rel="tooltip" title="" data-placement="bottom" data-html="true" data-original-title="Programado cuidadosamente para TV Tem.">
	                </div>
				</div>
	      	<div class="ripple-container"></div></a>
	    </div>

	    <div class="collapse navbar-collapse" id="navigation-index">
	    	<ul class="nav navbar-nav navbar-right">
				<li>
					<a href="#">
						Sobre
					</a>
				</li>

				<?php
					if (isLogged()){
				?>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<i class="material-icons">account_circle</i> Olá, <?=$_SESSION[SERVER_IDENT]['name']?>
							<div class="ripple-container"></div></a>
							<form action="dologout.php"  success="core.showNotification(core.requestData.msg, core.requestData.icon, core.requestData.type);setTimeout(function(){core.loadPage('<?=BASE_URL?>login');}, 2000);" fail="core.showNotification('Não foi possível fazer logout. (Código 1)', 'error', 'danger');" rest>
								<ul class="dropdown-menu dropdown-menu-right">
									<li class="dropdown-header">Conta</li>
									<li><a href="<?=BASE_URL?>chgpassword"><i class="material-icons">vpn_key</i> Alterar Senha</a></li>
									<li class="dropdown-header">Quizes</li>
									<li><a href="<?=BASE_URL?>"><i class="material-icons">list</i> Todos os Quizes</a></li>
									<?php if (isAdmin()){ ?>
									
									<li><a href="<?=BASE_URL?>me"><i class="material-icons">list</i> Meus Quizes</a></li>
									<li><a href="<?=BASE_URL?>creator/new"><i class="material-icons">add_box</i> Criar Novo</a></li> 
									
									<?php } ?>
									
									<li class="divider"></li>
									<li>
										<a href="#" submit><i class="material-icons">exit_to_app</i> Logout</a>
									</li>
								</ul>
							</form>
						</a>
					</li>
				<?php
					} else {
				?>
					<li>
						<a href="<?=BASE_URL?>login">
							<i class="material-icons">account_circle</i> Login
						</a>
					</li>
				<?php
					}

				?>
	    	</ul>
	    </div>
	</div>
</nav>

<!-- end navbar -->
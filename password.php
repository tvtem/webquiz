<?php

require_once('master/header.php');

?>

	<div class="wrapper">
		<div class="header header-filter" style="background-image: url('img/city.jpg'); background-size: cover; background-position: top center;">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">

						<div style="height: 100px;" class="hidden-xs"></div>

						<div class="card card-signup">
							<form class="form" action="changepass.php" success="core.showNotification(core.requestData.msg, core.requestData.icon, core.requestData.type);if (core.requestData.error != 'true'){setTimeout(function(){ core.loadPage('login.php'); }, 2000); }" fail="core.showNotification('Não foi possível fazer login. (Código 1)', 'error', 'danger');" rest>
								<div class="header header-primary text-center">
									<h4>Alterar Senha</h4>
								</div>
								<div class="col-xs-10 col-xs-offset-1">

									<div class="form-group label-floating">
										<label class="control-label">Usuário</label>
										<input type="text" name="user" class="form-control" required>
									</div>

									<div class="form-group label-floating">
										<label class="control-label">Senha Atual</label>
										<input type="password" name="password" class="form-control" required>
									</div>

									<div class="form-group label-floating">
										<label class="control-label">Nova Senha</label>
										<input type="password" name="npassword" class="form-control" required>
									</div>

									<div class="form-group label-floating">
										<label class="control-label">Repetir Nova Senha</label>
										<input type="password" name="rnpassword" class="form-control" required>
									</div>
								</div>
								<div class="footer text-center">
									<div class="input-group" style="width: 100%;padding: 0 31px 0 30px;">
										<p>&nbsp;</p>
										<input type="submit" class="btn btn-primary btn-block" value="Alterar Senha" />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>

<?php

require_once('master/footer.php');

?>
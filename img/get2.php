<?php

require_once('../lib/core.php');

$img = __($_GET['id']);
$filename = 'upload/' . $img;

if (file_exists($filename.'.jpg')) {
    $filename .= '.jpg';
}
if (file_exists($filename.'.jpeg')) {
    $filename .= '.jpeg';
}
if (file_exists($filename.'.png')) {
    $filename .= '.png';
}
if (file_exists($filename.'.gif')) {
    $filename .= '.gif';
}

$ext = pathinfo($filename, PATHINFO_EXTENSION);


$handle = fopen($filename, "rb");
$contents = fread($handle, filesize($filename));
fclose($handle);
 
header("content-type: image/".$ext);
 
echo $contents;

?>
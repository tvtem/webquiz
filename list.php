<?php

require_once('master/header.php');
prepareTemplate('default-page');

?>
<div class="header header-filter" style="background-image: url('img/examples/city.jpg');"></div>

<div class="main main-raised">
    <div class="profile-content">
        <div class="container">
            <div class="section" id="myQuiz">
                <center><h1>Meus Quiz</h1></center>
                <div class="row">
                <?php


                    $raw = execRaw("SELECT * FROM quiz ORDER BY updated_time DESC");
                    $quizzes = array();
                    while ($row = $raw->fetch_array()) {
                        array_push($quizzes, $row);
                    }

                    foreach ($quizzes as $quiz){
                        if (isset($quiz['content'])){
                            $ndata = $quiz['content'];
                            $data = json_decode($ndata, true);
                            ?>

                                <div class="col-xs-12">
                                    <div class="card card-nav-tabs">
                                        <div class="headering header-info">
                                            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
                                            <ul class="nav nav-tabs">
                                                <li>
                                                    <a href="./creator/<?=$quiz['id']?>/">
                                                        <i class="material-icons">mode_edit</i>
                                                        Editar
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="./reviewer/<?=$quiz['id']?>/">
                                                        <i class="material-icons">equalizer</i>
                                                        Ver Relatório
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="content">
                                            <div class="tab-content">
                                                <div class="tab-pane active">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-4">
                                                            <div class="dz-box">
                                                                <div class="bg-center dz-bg" style="background-image: url(<?=$data['cover_img']?>);">
                                                                    <img src="<?=BASE_URL?>img/placeholders/5x3.png" width="100%" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 col-sm-7">
                                                            <h3><?=$data['title']?></h3>
                                                            <p><?=$data['description']?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            <?php
                        }
                    }

                ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

require_once('master/footer.php');

?>